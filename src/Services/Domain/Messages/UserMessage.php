<?php

namespace EduCodeTest\Services\Domain\Messages;

class UserMessage {

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string|null
     */
    private $message;

    /**
     * UserMessage constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        if (empty($data['name'])) {
            throw new \InvalidArgumentException('Message missing name field');
        }
        $this->name = $data['name'];

        if (!filter_var($data['email'] ?? null, \FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException('Message missing e-mail field');
        }
        $this->email = $data['email'];

        $this->message = $data['message'] ?? null;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

}