<?php

namespace EduCodeTest\Support\Http;

class CurlHttpClient implements HttpClient {

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return array|mixed
     */
    public function request(string $method, string $url, array $options)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        if (strcasecmp($method, 'post') === 0) {
            curl_setopt($ch, CURLOPT_POST, 1);
        }

        if ($options['json'] ?? null) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json']);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        }

        if (!empty($options['data']) && is_array($options['data'])) {
            if ($options['json'] ?? null) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($options['data']));
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $options['data']);
            }
        }

        if (!empty($options['headers']) && is_array($options['headers'])) {
            foreach ($options['headers'] as $key => $value) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(sprintf('%s: %s', $key, $value)));
            }
        }
        $response = curl_exec($ch);
        $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return [$response, $httpStatusCode];
    }
}