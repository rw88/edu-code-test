<?php

namespace EduCodeTest\Services\Application\Messages;

use EduCodeTest\Services\Domain\Messages\UserMessage;
use EduCodeTest\Support\Http\HttpClient;
use EduCodeTest\Support\Translation\Translation as T;

class UserMessageService {

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * UserMessageService constructor.
     * @param HttpClient $httpClient
     */
    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param $message
     */
    public function sendMessage(UserMessage $message): void
    {

        $data = [
            'name' => $message->getName(),
            'email' => $message->getEmail(),
            'body' => $message->getMessage(),
        ];

        [, $httpCode] = $this->httpClient->request('post', 'https://jsonplaceholder.typicode.com/comments', [
            'json' => true,
            'data' => $data,
        ]);

        if ($httpCode != 201) {
            throw new \RuntimeException(T::trans("Unable to send the message. Please try again later"));
        }

    }

}
