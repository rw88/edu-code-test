<?php

namespace EduCodeTest\Support\Translation;

class Translation {

    /**
     * @param $text
     * @return string
     */
    public static function trans(string $text): string
    {
        return __($text, EDU_CODE_TEST_PLUGIN_BASE_NAME);
    }
}