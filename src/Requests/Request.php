<?php

namespace EduCodeTest\Requests;

abstract class Request {

    /**
     * @var bool
     */
    protected $validated = false;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var array
     */
    protected $rawData = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * Request constructor.
     * @param array $rawData
     */
    public function __construct(array $rawData)
    {
        $this->rawData = $rawData;
    }

    /**
     * @return bool
     */
    protected function valid(): bool
    {
        return empty($this->errors);
    }

    /**
     * @return Request
     */
    public function sanitize(): self
    {

        foreach ($this->rawData as $key => $value) {
            $this->data[$key] = filter_var($value, FILTER_SANITIZE_STRING);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function errors(): array
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function data(): array
    {
        return $this->data;
    }

}