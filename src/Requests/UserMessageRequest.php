<?php

namespace EduCodeTest\Requests;

use EduCodeTest\Support\Translation\Translation as T;

class UserMessageRequest extends Request {

    /**
     * @return bool
     */
    public function validate(): bool {

        if(empty($this->data['name'])) {
            $this->errors[] = T::trans('Please enter a valid name');
        }

        if(filter_var($this->data['email'] ?? null, FILTER_VALIDATE_EMAIL) === false) {
            $this->errors[] = T::trans('Please enter a valid e-mail');
        }

        return $this->valid();
    }

}