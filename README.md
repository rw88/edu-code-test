# Project Name

EDU Coding Test Plugin

## Installation

Clone the source code from the repository into wp-content/plugin's folder or zip the source code from the plugin root dir, so you can install it via WP admin Panel.

## Usage

In order to see the form, use the short-code [edu-code-test] in any part of the application.

## Development notes

I added file-based sessions for simplicity. For scalable applications, I would use a key-value database such as Redis as session handler.

## Credits

Robert Souza <robertwagnerxrw@gmail.com>