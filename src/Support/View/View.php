<?php

namespace EduCodeTest\Support\View;

use EduCodeTest\Support\Session\SessionInterface;
use EduCodeTest\Support\Translation\Translation;

class View {

    /**
     * @var string
     */
    private $templatesDirPath;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * View constructor.
     * @param string $templatesDirPath
     * @param SessionInterface $session
     */
    public function __construct(string $templatesDirPath, SessionInterface $session)
    {
        $this->templatesDirPath = $templatesDirPath;
        $this->session = $session;
    }

    public function load(string $templatePath): string
    {
        $session = $this->session;
        $errors = $this->session->getFlash('errors') ?? [];
        $input = $this->session->getFlash('form-input') ?? [];
        $translation = new Translation();

        ob_start();
        require $this->templatesDirPath . $templatePath;

        return ob_end_flush();
    }

}