<?php

use EduCodeTest\Controllers;

$routes = [];

/**
 * Route format: [httpMethod string, action string, [controllerFQCL string, controllerMethod string]]
 */

$routes[] = ['POST', 'edu_code_test_send_message', [Controllers\MessagesController::class, 'sendMessage']];

return $routes;