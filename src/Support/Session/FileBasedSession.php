<?php

namespace EduCodeTest\Support\Session;

class FileBasedSession implements SessionInterface {

    /**
     * @var string
     */
    private $prefix;

    /**
     * FlashMessages constructor.
     * @param string $prefix
     */
    public function __construct(string $prefix = "")
    {
        $this->prefix = $prefix;

    }

    public function init(): void
    {
        if (!session_id()) {
            session_start();
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return isset($_SESSION[$this->prefixedKey($key)]);
    }

    /**
     * @param string $key
     * @param $value
     */
    public function addFlash(string $key, $value)
    {
        $_SESSION[$this->prefixedKey($key)] = $value;
    }

    /**
     * @param string $key
     * @return null
     */
    public function getFlash(string $key)
    {

        $prefixedKey = $this->prefixedKey($key);

        $value = $_SESSION[$prefixedKey] ?? null;
        unset($_SESSION[$prefixedKey]);

        return $value;
    }

    /**
     * @param string $key
     * @return string
     */
    private function prefixedKey(string $key): string
    {
        return $this->prefix . $key;
    }
}