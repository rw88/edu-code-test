<?php

namespace EduCodeTest\Infrastructure\Boot;

use EduCodeTest\Support\Container\Container;
use EduCodeTest\Support\Session\SessionInterface;
use EduCodeTest\Support\Translation\Translation as T;

class Plugin {

    /**
     * @var Container
     */
    private static $container;

    /**
     * @var SessionInterface
     */
    private $session;


    public function __construct()
    {
        self::$container = ServicesInit::register();
        $this->session = self::$container->resolve('session');
    }

    public function register()
    {
        add_action('init', [$this, 'registerActions']);
        add_action('init', [$this, 'registerShortcodes']);
    }

    public function registerShortcodes(): void
    {

        add_shortcode('edu-code-test', function () {

            return self::$container->resolve('view')->load('messages/message.form.php');
        });
    }

    public function registerActions(): void
    {

        $container = self::$container;
        $routes = require plugin_dir_path(EDU_CODE_TEST_PLUGIN) . 'configs/routes.php';

        foreach ($routes as $route) {

            [$method, $key, $action] = $route;

            $callback = function () use ($key, $method, $action, $container) {

                if (strcasecmp($_SERVER['REQUEST_METHOD'] ?? '', $method) !== 0) {
                    return;
                }

                [$controller, $method] = $action;

                if (!isset($_POST['edu_nonce_check']) || !wp_verify_nonce($_POST['edu_nonce_check'], $key)) {
                    wp_die(T::trans('Invalid nounce code provided'));
                }

                $controllerInstance = new $controller($container);
                $controllerInstance->{$method}();
            };

            add_action('admin_post_nopriv_' . $key, $callback);
            add_action('admin_post_' . $key, $callback);

        }
    }

    public static function getContainer(): Container
    {
        return self::$container;
    }
}