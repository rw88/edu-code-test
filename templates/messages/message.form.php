<form method="post" action="<?= admin_url('admin-post.php'); ?>">

    <?php
    if ($session->has('message')) {
        printf('<span class="notice notice-success">%s</span>', $session->getFlash('message'));
    }
    ?>

    <?php wp_nonce_field('edu_code_test_send_message', 'edu_nonce_check'); ?>

    <input type="hidden" name="action" value="edu_code_test_send_message"/>

    <input type="text" name="name"
           placeholder="<?= $translation->trans('Enter your name'); ?>"
           required
           value="<?= $input['name'] ?? '' ?>"
    />

    <input type="email" name="email"
           placeholder="<?= $translation->trans('Enter your e-mail'); ?>"
           required
           value="<?= $input['email'] ?? '' ?>"
    />

    <textarea name="message"
              placeholder="<?= $translation->trans('Enter a message (optional)'); ?>"
    ><?= $input['message'] ?? '' ?></textarea>

    <button type="submit"><?= $translation->trans('Submit message'); ?></button>

    <?php require(__DIR__ . '/../errors/error-messages.php'); ?>
</form>