<?php

namespace EduCodeTest\Support\Session;

interface SessionInterface {

    /**
     * @param string $key
     * @param $data
     * @return mixed
     */
    public function addFlash(string $key, $data);

    /**
     * @param string $key
     * @return mixed
     */
    public function getFlash(string $key);

}