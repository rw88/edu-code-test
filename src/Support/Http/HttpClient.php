<?php

namespace EduCodeTest\Support\Http;

interface HttpClient {

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return mixed
     */
    public function request(string $method, string $url, array $options);

}