<?php
/**
 * Plugin Name:       EDU Code Test Assignment
 * Description:       Just a coding test assignment for https://medical.edu.mt.
 * Version:           1
 * Requires PHP:      7.2
 * Author:            Robert Souza
 * Author URI:        https://www.linkedin.com/in/robertwagnerxrw/
 * Text Domain:       edu-code-test
 */

namespace EduCodeTest;

defined('WPINC') or die('This plugin should be called within wordpress context.');

define('EDU_CODE_TEST_PLUGIN', __FILE__);
define('EDU_CODE_TEST_PLUGIN_BASE_NAME', 'edu-code-test');

require_once(__DIR__ . '/src/Support/Loader/Psr4Autoloader.php');

use EduCodeTest\Support\Loader\Psr4AutoloaderClass;
use EduCodeTest\Infrastructure\Boot\Plugin;

(function () {
    $loader = new Psr4AutoloaderClass();
    $loader->addNamespace('EduCodeTest', __DIR__ . '/src');
    $loader->register();

    $plugin = new Plugin();
    $plugin->register();
})();

