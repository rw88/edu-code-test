<?php

namespace EduCodeTest\Controllers;

use EduCodeTest\Requests\UserMessageRequest;
use EduCodeTest\Services\Application\Messages\UserMessageService;
use EduCodeTest\Services\Domain\Messages\UserMessage;
use EduCodeTest\Support\Container\Container;
use EduCodeTest\Support\Redirect\Redirect;

class MessagesController extends BaseController {

    /**
     * @var UserMessageService
     */
    private $userMessageService;

    /**
     * MessagesController constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);

        $this->userMessageService = $container->resolve(UserMessageService::class);
    }

    public function sendMessage()
    {
        $request = new UserMessageRequest($_POST ?? []);

        if (!$request->sanitize()->validate()) {
            return $this->redirectRequestBack($request);
        }

        try {
            $message = new UserMessage($request->data());
            $this->userMessageService->sendMessage($message);
        } catch (\RuntimeException $e) {
            $errors = [$e->getMessage()];
            return Redirect::back()
                ->withErrors($errors)
                ->withInput($request->data())
                ->go();
        }


        $this->session->addFlash('message', "The message was sent successfully");

        return Redirect::back()->go();

    }

}