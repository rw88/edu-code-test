<?php

namespace EduCodeTest\Support\Redirect;

use EduCodeTest\Infrastructure\Boot\Plugin;

class Redirect {

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var array
     */
    private $input = [];

    /**
     * Redirect constructor.
     * @param string $url
     */
    private function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return Redirect
     */
    public static function back(): self
    {
        return new Redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * @param array $errors
     * @return Redirect
     */
    public function withErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * @param array $input
     * @return Redirect
     */
    public function withInput(array $input): self
    {
        $this->input = $input;

        return $this;
    }

    public function go(): void
    {

        $session = Plugin::getContainer()->resolve('session');

        if (!empty($this->errors)) {
            $session->addFlash('errors', $this->errors);
        }

        if (!empty($this->input)) {
            $session->addFlash('form-input', $this->input);
        }

        wp_redirect($this->url);
    }

}