<?php

namespace EduCodeTest\Support\Container;

class Container {

    const REGULAR = 1;

    const SINGLETON = 2;

    /**
     * @var array
     */
    private $binds = [];

    /**
     * @var array
     */
    private $cached = [];

    /**
     * @var array
     */
    private $aliases = [];

    /**
     * @var string
     */
    private $lastBindedKey;

    /**
     * It binds a service to the container
     *
     * @param string $key
     * @param callable $value
     * @return Container
     */
    public function bind(string $key, callable $value): self
    {
        $this->binds[$key] = [self::REGULAR, $value];
        $this->lastBindedKey = $key;

        return $this;
    }

    /**
     * It maps an alias key to a binded key
     *
     * @param string $alias
     * @param string|null $key
     * @return Container
     */
    public function alias(string $alias, string $key = null): self
    {
        if (!$key && $this->lastBindedKey) {
            $key = $this->lastBindedKey;
            $this->lastBindedKey = null;
        }

        $this->aliases[$alias] = $key;

        return $this;
    }

    /**
     * It maps a singleton service to the container
     *
     * @param string $key
     * @param callable $value
     * @return Container
     */
    public function singleton(string $key, callable $value): self
    {
        $this->binds[$key] = [self::SINGLETON, $value];
        $this->lastBindedKey = $key;

        return $this;
    }

    /**
     * It resolves a service from the container
     *
     * @param string $key
     * @return mixed
     */
    public function resolve(string $key)
    {
        if (isset($this->binds[$key])) {
            return $this->loadService($key);
        }

        if (isset($this->aliases[$key])) {
            return $this->loadService($this->aliases[$key]);
        }

        throw new \RuntimeException('Unable to call ' . $key);
    }

    /**
     * It checks if a key exists
     *
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return isset($this->binds[$key]);
    }

    /**
     * It loads a service from the container
     *
     * @param string $key
     * @return mixed
     */
    private function loadService(string $key)
    {

        [$type, $callable] = $this->binds[$key];

        if (!is_callable($callable)) {
            throw new \RuntimeException('Unable to call ' . $key);
        }

        if ($type === self::SINGLETON) {
            if (isset($this->cached[$key])) {
                return $this->cached[$key];
            }

            $this->cached[$key] = $callable($this);

            return $this->cached[$key];
        }

        return $callable($this);
    }

}