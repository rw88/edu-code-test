<?php

namespace EduCodeTest\Infrastructure\Boot;

use EduCodeTest\Services\Application\Messages\UserMessageService;
use EduCodeTest\Support\Container\Container;
use EduCodeTest\Support\Http\CurlHttpClient;
use EduCodeTest\Support\Http\HttpClient;
use EduCodeTest\Support\Session\FileBasedSession;
use EduCodeTest\Support\Session\SessionInterface;
use EduCodeTest\Support\View\View;

class ServicesInit {

    public static function register(): Container
    {

        $container = new Container();

        $container
            ->singleton(SessionInterface::class, function () {
                $session = new FileBasedSession("educodetest_");
                $session->init();

                return $session;
            })
            ->alias('session');


        $container
            ->singleton(View::class, function (Container $c) {
                return new View(plugin_dir_path(EDU_CODE_TEST_PLUGIN) . 'templates/', $c->resolve('session'));
            })
            ->alias('view');


        $container
            ->singleton(HttpClient::class, function (Container $c) {
                return new CurlHttpClient();
            });

        $container
            ->bind(UserMessageService::class, function (Container $c) {
                return new UserMessageService($c->resolve(HttpClient::class));
            });

        return $container;

    }

}