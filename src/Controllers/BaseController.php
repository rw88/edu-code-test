<?php

namespace EduCodeTest\Controllers;

use EduCodeTest\Support\Container\Container;
use EduCodeTest\Requests\Request;
use EduCodeTest\Support\Redirect\Redirect;
use EduCodeTest\Support\Session\SessionInterface;

class BaseController {

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * BaseController constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->session = $container->resolve('session');
    }

    /**
     * @param Request $request
     */
    protected function redirectRequestBack(Request $request)
    {
        return Redirect::back()
            ->withErrors($request->errors())
            ->withInput($request->data())
            ->go();
    }

}
